..
    :copyright: Copyright (c) 2016 ftrack

********
Glossary
********

.. glossary::

    promise

        Promise objects are used for asynchronous computations. A Promise
        represents an operation that hasn't completed yet, but is expected in
        the future.

        Read more on `MDN <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise>`_.

    nodejs

        Node.js is a JavaScript runtime built on Chrome V8 JavaScript engine. It
        is used to build a distribution of the ftrack API.