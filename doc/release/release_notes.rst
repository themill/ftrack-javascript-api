..
    :copyright: Copyright (c) 2016 ftrack

.. _release/release_notes:

*************
Release Notes
*************

.. release:: 0.1.0
    :date: 2016-06-13

    .. change:: new

        Initial release with support for query, create, update and delete
        operations.
